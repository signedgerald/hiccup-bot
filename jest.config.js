module.exports = async () => {
	return {
		verbose: true,
		testEnvironment: 'node',
		rootDir: './'
	};
};