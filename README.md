# README

for-fun bot with few custom commands

- anime search may not work depending on API

## Instructions to run locally

### Prerequisites

- npm/node

### Commands

- `npm i` to install modules required to run (only run this once)
- `node .` to start
- `!help` for further information on available commands
