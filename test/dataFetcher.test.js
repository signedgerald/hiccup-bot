// jest.mock('node-fetch', () => ({
//   fetch: jest.fn().mockName('fetch');
// }));
jest.mock('node-fetch');
// jest.mock('discord.js');
// jest.mock('jsdom');
const { fetchData } = require('../dataFetcher');

describe('test suite for dataFetcher.js', () => {
	it('test fetchData function on error', async () => {
		console.log = jest.fn();
		try {
			await fetchData()
		} catch(err) {
			expect(console.log).toHaveBeenCalledWith('Unable to fetch data');
		}
	});
  
	it('test fetchData function on success, enriched message array is returned', async () => {
    
	})
})