const parseImages = (text) => {
	const images = text.window.document.querySelectorAll('img');
	const imagesArray = [];
	images.forEach((image) => {
		imagesArray.push(image.getAttribute('src'));
	});
	return imagesArray;
};

const parseLinks = (text) => {
	const links = text.window.document.querySelectorAll('a');
	const filteredLinksArray = [];
	for (let i = 1; i < links.length; i+= 2){
		filteredLinksArray.push(links[i].getAttribute('href'));
	}
	return filteredLinksArray;
};

module.exports = {
	parseImages,
	parseLinks
};