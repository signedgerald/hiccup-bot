const fs = require('fs');

const increment = (message) => {
	message.delete();
	const currentDate = new Date();
	const cDate =
					currentDate.getFullYear() +
					'-' +
					(currentDate.getMonth() + 1) +
					'-' +
					currentDate.getDate();
	const count = JSON.parse(fs.readFileSync(__dirname + '/counts.json', 'utf8'));

	const readDate = count.date;
	let dailyCount;

	if (readDate.toString() == cDate.toString()) {
		console.log('Same day, continuing count');
		dailyCount = count.today;
	} else {
		console.log('New day, beginning count');
		dailyCount = 0;
	}
	let totalCount = count.total;

	dailyCount += 1;
	totalCount += 1;
	const output = {
		date: cDate,
		today: dailyCount,
		total: totalCount,
	};
	fs.writeFileSync(__dirname + '/counts.json', JSON.stringify(output));
	message.channel.send(
		dailyCount + ' hiccups today... <:FeelsWeirdMan:694809374018895945>'
	);
};

module.exports = {
	name: '',
	description: 'increment counter',
	increment,
	execute(message) {
		increment(message);
	}
};