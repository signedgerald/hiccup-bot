const fs = require('fs');

const total = (message) => {
	message.delete();
	const count = JSON.parse(fs.readFileSync(__dirname + '/counts.json', 'utf8'));
	const totalCount = count.total;
	message.channel.send(
		totalCount +
        ' hiccups since I started counting... <:FeelsWeirdMan:694809374018895945>'
	);
};

module.exports = {
	name: 'total',
	description: 'show total counted so far',
	total,
	execute(message) {
		total(message);
	}
};