const fs = require('fs');

const decrement = (message) => {
	if (message.content === '!!') {
		message.delete();
		const count = JSON.parse(fs.readFileSync(__dirname + '/counts.json', 'utf8'));
		let dailyCount = count.today;
		let totalCount = count.total;
		dailyCount -= 1;
		totalCount -= 1;

		const output = {
			date: count.date,
			today: dailyCount,
			total: totalCount,
		};
		fs.writeFileSync(__dirname + '/counts.json', JSON.stringify(output));
		message.channel.send(
			dailyCount + ' hiccups today... <:FeelsWeirdMan:694809374018895945>'
		);
	}
};

module.exports = {
	name: 'decrement',
	description: 'decrement counter',
	aliases: ['!'],
	decrement,
	execute(message) {
		decrement(message);
	}
};