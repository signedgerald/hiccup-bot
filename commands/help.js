const prefix = '!';
const help = (message) => {
	const helpCommand = {
		title: 'Commands',
		fields: [
			{
				name: prefix,
				value: 'increments hiccup counter',
			},
			{
				name: '!!',
				value: 'decrements hiccup counter',
			},
			{
				name: prefix + 'total',
				value: 'shows total hiccups counted so far',
			},
			{
				name: prefix + 'ms',
				value: 'link to metastream',
			},
			{
				name: prefix + 'cm <no.>',
				value: 'clears no. amount messages if specified, if not 100 is default',
			},
		],
	};
	message.channel.send({ embed: helpCommand });
};

module.exports = {
	name: 'help',
	description: 'List of commands',
	help,
	execute(message) {
		help(message);
	}
};