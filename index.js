const Discord = require('discord.js');
const fs = require('fs');
const { prefix, token } = require('./config.json');

const client = new Discord.Client();
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

const { phungyReact } = require('./emoteReact');
const { fetchData } = require('./dataFetcher');

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

async function mkbcat() {
	const fetchedData = await fetchData();
	const textChannel = client.channels.cache.get('769147432965505044');
	fetchedData.forEach((data) => {
		textChannel.send(data);
	});
	setTimeout(mkbcat, 60 * 30 * 1000);
};

client.on('ready', async () => {
	console.log(`Logged in as ${client.user.tag}!`);
	client.user.setActivity('hiccups', { type: 'LISTENING' });
	// mkbcat();
});

client.on('voiceStateUpdate', (oldState, newState) => {
	// general text channel
	const textChannel = client.channels.cache.get('137369589829861376');
	const logsChannel = client.channels.cache.get('695061073967054939');

	const oldVoice = oldState.channelID;
	const newVoice = newState.channelID;
	if (oldVoice != newVoice) {
		if (oldVoice == null) {
			// ask Alex what he has to complain about today
			if (newState.member.id == '137371251441270784') {
				textChannel.send(
					`<@${newState.member.id}> ok so what u got to complain about today`
				);
			}
			if (newState.member.id == '137627973720211456') {
				textChannel.send(`<@${newState.member.id}> has ulted into the server`);
			}
			logsChannel
				.send(newState.member.id)
				.then((sentMessage) =>
					sentMessage.edit(
						`<@${newState.member.id}> joined the channel ${newState.channel.name}`
					)
				);
		} else if (newVoice == null) {
			logsChannel
				.send(`${oldState.member.id}> has disconnected from the server`)
				.then((sentMessage) =>
					sentMessage.edit(
						`<@${oldState.member.id}> has disconnected from the server`
					)
				);
		} else if (newVoice && oldVoice) {
			logsChannel
				.send(`${newState.member.id} switched to ${newVoice}`)
				.then((sentMessage) =>
					sentMessage.edit(
						`<@${newState.member.id}> switched to ${newState.channel.name}`
					)
				);
		}
	}
});

client.on('message', async (message) => {
	console.log(message);
	if (!message.content.startsWith(prefix) || message.author.bot) return;
	const args = message.content.slice(prefix.length).trim().split(/ +/);
	const commandName = args.shift().toLowerCase();
	const command = client.commands.get(commandName) || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
	if (!command) return;
	
	phungyReact(message);
  
	try {
		command.execute(message, args);
	} catch (err) {
		console.error(err);
		message.reply('there was an error trying to execute that command');
	}
});

client.login(token);
