const fetch = require('node-fetch');
const jsdom = require('jsdom');
const Discord = require('discord.js');
const { parseImages, parseLinks } = require('./parseData');

const { JSDOM } = jsdom;

const fetchData = async () => {
	try {
		const response = await fetch('https://mkbcat.xyz');
		const text = await response.text();
		const parsedText = new JSDOM(text);
		const returnedImages = parseImages(parsedText);
		const returnedLinks = parseLinks(parsedText);
		const returnedMessageArray = [];
		for(let i = 0; i < returnedLinks.length; i++ ) {
			const enrichedMessage = new Discord.MessageEmbed()
				.setTitle(`${returnedLinks[i]}`)
				.setImage(returnedImages[i])
				.setURL(returnedLinks[i]);
			returnedMessageArray.push(enrichedMessage);
		}
		return returnedMessageArray;
	} catch (err) {
		console.log('Unable to fetch data');
		throw err;
	}
};

module.exports = {
	fetchData
};